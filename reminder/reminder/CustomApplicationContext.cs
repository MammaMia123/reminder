﻿using reminder.Model;
using reminder.ViewModel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using WPFViews;

namespace reminder
{
    public class CustomApplicationContext : ApplicationContext
    {
        private NotifyIcon notifyIcon;
        private readonly List<Reminder> events = new List<Reminder>()
        {
            new Reminder("Prad", "", new DateTime(2019, 12, 10), ReminderType.home),
            new Reminder("VAT", "Zaplac vat za luty", new DateTime(2019, 10, 9), ReminderType.infakt),
            new Reminder("B-day", "Zawsze sa czyjes urodziny", DateTime.Now, ReminderType.other),
            new Reminder("Paliwo", "Zatakuj zanim sie skonczy paliwo, albo bedziesz wracac na piechote", new DateTime(2019,05,05), ReminderType.car)
        };

        public CustomApplicationContext()
        {
            InitializeContext();
        }

        private void InitializeContext()
        {
            notifyIcon = new NotifyIcon()
            {
                Icon = new Icon("reminder.ico"),
                Visible = true
            };

            CreateContextMenuNotifyIcon();

            notifyIcon.MouseUp += NotifyIcon_Click;
        }

        private void CreateContextMenuNotifyIcon()
        {
            notifyIcon.ContextMenuStrip = new ContextMenuStrip();
            notifyIcon.ContextMenuStrip.Items.Add(CreateContextMenuItemWithHandler("Exit", ExitApplication_Click));
        }

        private ToolStripMenuItem CreateContextMenuItemWithHandler(string name, EventHandler eventHandler)
        {
            return new ToolStripMenuItem(name, null, eventHandler);
        }

        private void NotifyIcon_Click(object sender, MouseEventArgs eventArgs)
        {
            if (eventArgs.Button == MouseButtons.Left)
            {
                PreviewEvents previewEvents = new PreviewEvents
                {
                    DataContext = new PreviewEventsViewModel(events)
                };

                previewEvents.Show();
                previewEvents.Activate();
            }
            if (eventArgs.Button == MouseButtons.Right)
            {
                MethodInfo mi = typeof(NotifyIcon).GetMethod("ShowContextMenu", BindingFlags.Instance | BindingFlags.NonPublic);
                mi.Invoke(notifyIcon, null);
            }
        }

        private void ExitApplication_Click(object sender, EventArgs eventArgs)
        {
            ExitThread();   
        }

        protected override void ExitThreadCore()
        {
            // clean forms ...

            notifyIcon.Visible = false;
            base.ExitThreadCore();
        }

    }
}
