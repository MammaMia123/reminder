﻿using reminder.Model;
using System;
using System.Collections.Generic;

namespace reminder.ViewModel
{
    /// <summary>
    /// ViewModel of single event (reminder) to remind displaying in PreviewEvents window
    /// </summary>
    public class ReminderViewModel
    {
        #region Members

        private string title;
        private string message;
        private DateTime date;
        private ReminderType type;

        private Dictionary<ReminderType, string> reminderIcons = new Dictionary<ReminderType, string>()
        {
            { ReminderType.car, "/WPFViews;component/Icons/car.png" },
            { ReminderType.home, "/WPFViews;component/Icons/home.png" },
            { ReminderType.infakt, "/WPFViews;component/Icons/infakt.png" },
            { ReminderType.other, "/WPFViews;component/Icons/other.png" }
        };

        #endregion

        public ReminderViewModel(Reminder reminder)
        {
            Title = reminder.Title;
            Message = reminder.Message;
            Date = reminder.Date;
            type = reminder.Type;
        }

        #region Properties

        public string Title
        {
            get { return title; }
            set
            {
                if (title != value)
                {
                    title = value;
                }
            }
        }

        public string Message
        {
            get { return message; }
            set
            {
                if (message != value)
                {
                    message = value;
                }
            }
        }

        public DateTime Date
        {
            get { return date; }
            set
            {
                if (date != value)
                {
                    date = value;
                }
            }
        }

        public int Days
        {
            get { return (date - DateTime.Now).Days; }
        }

        public string IconPath
        {
            get { return reminderIcons[type]; }
        }

        #endregion

    }

}
