﻿using reminder.Model;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Data;
using System.Linq;

namespace reminder.ViewModel
{
    /// <summary>
    /// ViewModel of PreviewEvents popup window
    /// showing all incoming events sorted by date (from sooner to later)
    /// </summary>
    public class PreviewEventsViewModel
    {
        public PreviewEventsViewModel(IEnumerable<Reminder> _events)
        {
            List<ReminderViewModel> viewModels = new List<ReminderViewModel>();
            _events.ToList().ForEach(p => viewModels.Add(new ReminderViewModel(p)));
            Events = (CollectionView)CollectionViewSource.GetDefaultView(viewModels);
            Events.SortDescriptions.Add(new SortDescription("Days", ListSortDirection.Ascending));
        }

        #region Properties

        public CollectionView Events
        {
            get;
        }

        // TODO update when add multiple languages

        /// <summary>
        /// Value returning in Message box of event in 
        /// PreviewEvents window if user did not enter any message for it
        /// </summary>
        public string NoMessage
        {
            get { return "(no message)"; }
        }

        /// <summary>
        /// String of hyperlink to open reminder window from PreviewEvents window
        /// </summary>
        public string OpenReminder
        {
            get { return "Open reminder..."; }
        }

        /// <summary>
        /// String used to describe remaining days
        /// </summary>
        public string DaysLeft
        {
            get { return "day(s) left"; }
        }

        #endregion
    }
}
