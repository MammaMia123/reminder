﻿using System;

namespace reminder.Model
{
    /// <summary>
    /// Model of single event (reminder) to remind
    /// </summary>
    public class Reminder
    {
        public Reminder(string title, string message, DateTime date, ReminderType reminderType)
        {
            Title = title;
            Message = message;
            Date = date;
            Type = reminderType;
        }

        public string Title { get; set; }
        public string Message { get; set; }
        public DateTime Date { get; set; }
        public ReminderType Type { get; set; }
    }

    public enum ReminderType
    {
        home,
        car,
        infakt,
        other
    }
}
