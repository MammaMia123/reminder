﻿using System;
using System.Windows;

namespace WPFViews
{
    /// <summary>
    /// Logika interakcji dla klasy PreviewEvents.xaml
    /// </summary>
    public partial class PreviewEvents : Window
    {
        public PreviewEvents()
        {
            InitializeComponent();

            this.WindowStartupLocation = WindowStartupLocation.Manual;
            this.Left = SystemParameters.WorkArea.Width - this.Width - 5;
            this.Top = SystemParameters.WorkArea.Height - this.Height - 5;
        }

        private void Window_Deactivated(object sender, EventArgs e)
        {
            Close();
        }
    }
}
