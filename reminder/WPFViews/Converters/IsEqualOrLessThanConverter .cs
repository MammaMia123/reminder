﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace WPFViews.Converters
{
    /// <summary>
    /// Converter which check if binded value is equal or less than provided value
    /// </summary>
    public class IsEqualOrLessThanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int intValue = (int)value;
            int compareToValue = System.Convert.ToInt32(parameter);

            return intValue <= compareToValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
